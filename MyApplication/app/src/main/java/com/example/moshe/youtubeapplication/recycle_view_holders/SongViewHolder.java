package com.example.moshe.youtubeapplication.recycle_view_holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.example.moshe.youtubeapplication.R;
import com.example.moshe.youtubeapplication.interfaces.AppContract;
import com.example.moshe.youtubeapplication.model.ListItem;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by moshe on 08/02/2017.
 */

public class SongViewHolder extends ChildViewHolder {
    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */
    @Nullable
    @BindView(R.id.song_row_song_id)
    TextView _songTitleTV;
    @Nullable @BindView(R.id.song_row_thumb_id)
    ImageView _songThumbIV;
    @Nullable @BindView(R.id.card_view)
    CardView _cardView;

    private Context _context;

    public SongViewHolder(Context context , @NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this._context = context;
    }

    public void bind(final ListItem listItem, final AppContract.ProvideData provideData) {
        _songTitleTV.setText(listItem.getTitle());
        Picasso.with(_context).load(listItem.getThumb()).into(_songThumbIV);
        _cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(provideData != null){
                    provideData.onSongPicked(listItem.getLink());
                }
            }
        });
    }
}
