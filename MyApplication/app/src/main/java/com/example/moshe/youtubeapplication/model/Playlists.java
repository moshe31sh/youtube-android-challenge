package com.example.moshe.youtubeapplication.model;

import android.os.Parcel;
import android.os.Parcelable;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by moshe on 07/02/2017.
 */


public class Playlists implements Parcelable {
    private ArrayList<Playlist> playlists = null;

    protected Playlists(Parcel in) {
        playlists = in.createTypedArrayList(Playlist.CREATOR);
    }

    public static final Creator<Playlists> CREATOR = new Creator<Playlists>() {
        @Override
        public Playlists createFromParcel(Parcel in) {
            return new Playlists(in);
        }

        @Override
        public Playlists[] newArray(int size) {
            return new Playlists[size];
        }
    };

    @JsonProperty("Playlists")
    public ArrayList<Playlist> getPlaylists() {
        return playlists;
    }

    @JsonProperty("Playlists")
    public void setPlaylists(ArrayList<Playlist> playlists) {
        this.playlists = playlists;
    }


    public Playlists() {
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(playlists);
    }

}
