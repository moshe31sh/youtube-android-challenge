package com.example.moshe.youtubeapplication.presnter;

import android.content.Context;
import android.content.Intent;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.moshe.youtubeapplication.activities.YoutubeActivity;
import com.example.moshe.youtubeapplication.helpers.JsonSerializationHelper;
import com.example.moshe.youtubeapplication.helpers.WebService;
import com.example.moshe.youtubeapplication.interfaces.AppContract;
import com.example.moshe.youtubeapplication.model.Playlists;
import com.example.moshe.youtubeapplication.utils.AppConst;

import org.json.JSONObject;

import java.io.IOException;


/**
 * Created by moshe on 07/02/2017.
 */

public class MainPresenter implements AppContract.Init {
    private Context _context;

    private AppContract.ProvideData provideData;

    public MainPresenter(Context context, AppContract.ProvideData activity) {
        this._context = context;
        this.provideData = activity;
    }

    /**
     * get song list from api
     */
    @Override
    public void GetSongsUrl() {
        WebService.getInstance(_context).getData(AppConst.URL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    provideData.setPlayLists((Playlists) JsonSerializationHelper.StringToObject(response.toString(), Playlists.class));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {

                }
            }
        });
    }

    @Override
    public void play(String song) {
        Intent intent = new Intent(_context, YoutubeActivity.class);
        intent.putExtra(AppConst.SONG, song.substring(song.indexOf("=") + 1, song.length()));
        _context.startActivity(intent);
    }

}
