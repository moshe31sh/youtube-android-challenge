package com.example.moshe.youtubeapplication.helpers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.moshe.youtubeapplication.interfaces.IWebService;

import org.json.JSONObject;

/**
 * Created by moshe on 07/02/2017.
 */

/**
 *
 */
public class WebService implements IWebService {
    private RequestQueue _requestQueue;
    private Context _context;
    private static WebService _webService;
    public static final String TAG = WebService.class.getSimpleName();

    public static WebService getInstance(Context context) {
        if (_webService == null) {
            _webService = new WebService(context);
        }
        return _webService;
    }

    /**
     *
     * @param context
     */
    private WebService(Context context) {
        this._context = context;
        _requestQueue = Volley.newRequestQueue(_context.getApplicationContext());
    }

    /**
     * get json from url
     * @param url
     * @param listener
     * @param errorListener
     */
    @Override
    public void getData(String url, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url,listener,errorListener);
        _requestQueue.add(jsonObjectRequest);

    }
    @Override
    public void cancel(){
        _requestQueue.cancelAll(TAG);
    }
}
