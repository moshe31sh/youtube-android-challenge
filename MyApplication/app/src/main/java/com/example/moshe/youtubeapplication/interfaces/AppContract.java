package com.example.moshe.youtubeapplication.interfaces;

import com.example.moshe.youtubeapplication.model.Playlists;

import java.util.ArrayList;

/**
 * Created by moshe on 07/02/2017.
 */

public interface AppContract {

     interface Init{
       void GetSongsUrl();
         void play(String song);
    }

    interface ProvideData{
        void setPlayLists(Playlists playlists);
        void onSongPicked(String song);

    }


}
