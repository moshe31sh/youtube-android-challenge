package com.example.moshe.youtubeapplication.recycle_view_holders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.example.moshe.youtubeapplication.R;
import com.example.moshe.youtubeapplication.interfaces.AppContract;
import com.example.moshe.youtubeapplication.interfaces.ISongPosition;
import com.example.moshe.youtubeapplication.model.ListItem;
import com.example.moshe.youtubeapplication.model.Playlist;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by moshe on 08/02/2017.
 */

public class YoutubeAdapter extends ExpandableRecyclerAdapter<Playlist, ListItem, ArtistViewHolder, SongViewHolder> implements ISongPosition{
    private LayoutInflater _nflater;
    private Context _context;
    private AppContract.ProvideData _provideData;
    private RecyclerView _recyclerView;

    public YoutubeAdapter(Context context, @NonNull List<Playlist> parentList,AppContract.ProvideData provideData,RecyclerView recyclerView) {
        super(parentList);
        _nflater = LayoutInflater.from(context);
        _context = context;
        _provideData = provideData;
        _recyclerView = recyclerView;
    }

    @NonNull
    @Override
    public ArtistViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View artistView = _nflater.inflate(R.layout.artist_row, parentViewGroup, false);
        return new ArtistViewHolder(artistView,this);
    }

    @NonNull
    @Override
    public SongViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View songView = _nflater.inflate(R.layout.songs_row, childViewGroup, false);
        return new SongViewHolder(_context,songView);
    }

    @Override
    public void onBindParentViewHolder(@NonNull ArtistViewHolder parentViewHolder, int parentPosition, @NonNull Playlist parent) {
        parentViewHolder.bind(parent);
    }

    @Override
    public void onBindChildViewHolder(@NonNull SongViewHolder childViewHolder, int parentPosition, int childPosition, @NonNull ListItem child) {
        childViewHolder.bind(child,_provideData);
    }



    public void setData(ArrayList<Playlist> songs) {
        for (int i = 0; i < songs.size(); i++) {
            getParentList().add(songs.get(i));
        }
        notifyDataSetChanged();
    }

    @Override
    public void calcPosition(String title) {
        int parentPos = 1;
        for(int i = 0 ; i < getParentList().size() ; i++){
            parentPos += getParentList().get(i).getChildList().size();
            if(getParentList().get(i).getListTitle().equals(title)){
                break;
            }
        }
        _recyclerView.scrollToPosition(parentPos);
    }
}
