package com.example.moshe.youtubeapplication.helpers;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by moshe on 07/02/2017.
 */

public class JsonSerializationHelper {
    private static ObjectMapper _mapper = new ObjectMapper();
    static {
        _mapper.configure(
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        _mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.NON_PRIVATE);
        _mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }
    public static <T> Object StringToObject(String input, TypeReference<T> responseType) throws IOException {
        return  _mapper.readValue(input, responseType);
    }

    /**
     * parse json to object
     * @param input
     * @param clazz
     * @return
     * @throws IOException
     */
    public static Object StringToObject(String input, Class clazz) throws IOException {
        return _mapper.readValue(input, clazz);
    }
}
