package com.example.moshe.youtubeapplication.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;


import com.example.moshe.youtubeapplication.R;
import com.example.moshe.youtubeapplication.helpers.WebService;
import com.example.moshe.youtubeapplication.interfaces.AppContract;
import com.example.moshe.youtubeapplication.model.Playlist;
import com.example.moshe.youtubeapplication.model.Playlists;
import com.example.moshe.youtubeapplication.presnter.MainPresenter;
import com.example.moshe.youtubeapplication.recycle_view_holders.YoutubeAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements AppContract.ProvideData {
    @BindView(R.id.recycle_view_youtube_activity_id)
    RecyclerView _recyclerView;

    private MainPresenter _mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        _mainPresenter = new MainPresenter(this, this);
        _mainPresenter.GetSongsUrl();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        WebService.getInstance(this).cancel();
    }


    @Override
    public void setPlayLists(Playlists playlists) {
        YoutubeAdapter youtubeAdapter = new YoutubeAdapter(this, playlists.getPlaylists(),this,_recyclerView);
        _recyclerView.setAdapter(youtubeAdapter);
        _recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public void onSongPicked(String song) {
        _mainPresenter.play(song);
    }
}
