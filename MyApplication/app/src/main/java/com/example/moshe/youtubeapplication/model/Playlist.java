package com.example.moshe.youtubeapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by moshe on 07/02/2017.
 */


public class Playlist implements Parcelable , Parent<ListItem> {

    private String listTitle;
    private ArrayList<ListItem> listItems = null;


    protected Playlist(Parcel in) {
        listTitle = in.readString();
        this.listItems = in.createTypedArrayList(ListItem.CREATOR);

    }

    public static final Creator<Playlist> CREATOR = new Creator<Playlist>() {
        @Override
        public Playlist createFromParcel(Parcel in) {
            return new Playlist(in);
        }

        @Override
        public Playlist[] newArray(int size) {
            return new Playlist[size];
        }
    };

    @JsonProperty("ListTitle")
    public String getListTitle() {
        return listTitle;
    }

    @JsonProperty("ListTitle")
    public void setListTitle(String listTitle) {
        this.listTitle = listTitle;
    }

    @JsonProperty("ListItems")
    public ArrayList<ListItem> getListItems() {
        return listItems;
    }

    @JsonProperty("ListItems")
    public void setListItems(ArrayList<ListItem> listItems) {
        this.listItems = listItems;
    }


    public Playlist() {
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(listTitle);
        dest.writeTypedList(listItems);
    }


    @Override
    public List<ListItem> getChildList() {
        return listItems;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
