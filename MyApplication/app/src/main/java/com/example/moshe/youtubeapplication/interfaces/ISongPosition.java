package com.example.moshe.youtubeapplication.interfaces;

/**
 * Created by moshe on 08/02/2017.
 */

public interface ISongPosition {
    void calcPosition(String title);
}
