package com.example.moshe.youtubeapplication.recycle_view_holders;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.example.moshe.youtubeapplication.R;
import com.example.moshe.youtubeapplication.interfaces.ISongPosition;
import com.example.moshe.youtubeapplication.model.Playlist;
import com.example.moshe.youtubeapplication.model.Playlists;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by moshe on 08/02/2017.
 */

public class ArtistViewHolder extends ParentViewHolder {
    /**
     * Default constructor.
     *
     * @param itemView The {@link View} being hosted in this ViewHolder
     */
    @Nullable @BindView(R.id.artist_row_name)
    TextView _artistNameTV;

    private ISongPosition _songPosition;
    private String _title;

    public ArtistViewHolder(@NonNull View itemView,ISongPosition songPosition ) {

        super(itemView);
        ButterKnife.bind(this,itemView);
        _songPosition = songPosition;
    }

    public void bind(Playlist playlist) {
        _artistNameTV.setText(playlist.getListTitle());
        _title = playlist.getListTitle();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        _songPosition.calcPosition(_title);
    }
}
