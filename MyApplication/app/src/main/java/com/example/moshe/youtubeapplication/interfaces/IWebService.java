package com.example.moshe.youtubeapplication.interfaces;

import com.android.volley.Response;

import org.json.JSONObject;

/**
 * Created by moshe on 07/02/2017.
 */

public interface IWebService {
    void getData(String url, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener);
    public void cancel();
}
